# Installation
> `npm install --save @types/min-document`

# Summary
This package contains type definitions for min-document ( https://github.com/Raynos/min-document ).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/min-document

Additional Details
 * Last updated: Fri, 26 Apr 2019 01:02:31 GMT
 * Dependencies: none
 * Global values: none

# Credits
These definitions were written by Ifiok JR. <https://github.com/ifiokjr>.
